#Serial to netcat VM - buildroot config

## Prerequisites
### Debian / Ubuntu-based
Minimum (build a bzImage that works in qemu): `sudo apt-get install make g++ unzip`

Recommended: (build a bootable image/vdi): `sudo apt-get install make g++ unzip kpartx qemu-utils`

##Building with Docker
**Needs 5-8GB of space, plus Docker using the BTRFS driver**

`sudo docker build https://raw.githubusercontent.com/voltagex/serial-vm-buildroot/master/Dockerfile`

## Building manually
**Needs approximately 3.2GB of space, takes 30 minutes depending on your computer and network speed**

First clone `buildroot` with `git clone --depth=1 https://github.com/buildroot/buildroot`
Then clone this repository with `git clone https://github.com/voltagex/serial-vm-buildroot`

Then:
```bash
cd serial-vm-buildroot
make serial_defconfig && make
```

When it's done there will be a bzImage in build/images. 

Buildroot's post image script will need sudo to use kpartx and parted.

Read the article: http://blog.voltagex.org/2015/07/29/fixing-a-buggy-windows-driver-with-a-virtual-machine/
