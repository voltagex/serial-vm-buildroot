#!/bin/bash
set -o nounset

command -v parted >/dev/null 2>&1 || { echo "I require parted but it's not installed.  Aborting." >&2; exit 1; }
command -v kpartx >/dev/null 2>&1 || { echo "I require kpartx but it's not installed.  Aborting." >&2; exit 1; }
command -v qemu-img >/dev/null 2>&1 || { echo "I require qemu-img but it's not installed.  Aborting." >&2; exit 1; }
if [ $EUID != 0 ]; then
    echo "This script needs to be run as root, enter your password if prompted"
    sudo -E "$0" "$@"
    exit $?
fi

BINARIES_DIR=$BASE_DIR/images/
image=$BINARIES_DIR/bootable.img
kernel=$BINARIES_DIR/bzImage

extlinux_path=$BR2_EXTERNAL/scripts/extlinux

if [ ! -f $kernel ]; then
	echo "Kernel not found at $kernel - do you need to run make first? This script is not designed to be run directly"
	exit 1
fi

mountpoint=$(mktemp -d)
dd if=/dev/zero of=$image bs=1M count=20
sync

parted $image -s "mklabel msdos mkpart primary ext2 0 100% set 1 boot on quit"
sync

kpartx=$(kpartx -avs $image)
new_partition=$(echo $kpartx | awk '{print $3}')

mkfs.ext2 /dev/mapper/$new_partition
mount /dev/mapper/$new_partition $mountpoint
pushd $mountpoint > /dev/null
cp $kernel .
mkdir $mountpoint/boot
$extlinux_path --install $mountpoint/boot

cat <<-EOF > $mountpoint/boot/extlinux.conf
 LABEL buildroot
 KERNEL /bzImage
 DEFAULT buildroot
EOF

popd > /dev/null

sync
umount $mountpoint
kpartx -d $image

echo
echo
echo

pushd $BINARIES_DIR > /dev/null
qemu-img convert -f raw $image -O vdi bootable.vdi

echo Built the following bootable images:
file $image
file bootable.vdi

popd > /dev/null

rm -r $mountpoint
echo done!
